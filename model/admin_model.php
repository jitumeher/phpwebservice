<?php
class Admin_model extends Model {
	function __construct() {
		parent::__construct ();
		
	}
	public function adminLogin() {
		$sth = $this->db->prepare ( "SELECT `ID` FROM `admin_table` where
				ADMIN_ID=:adminid and PASSWORD=:password" );
		$sth->execute ( array (
				':adminid' => $_POST ['adminId'],
				':password' => $_POST ['password'] 
		) );
		$count = $sth->rowCount ();
		return $count;
	}
	public function editUser($Id) {
		$sth = $this->db->prepare ( "SELECT * FROM `user_table` where
				ID=:Id" );
		$sth->execute ( array (
				':Id' => $Id 
		) );
		$data = $sth->fetchAll ();
		return $data;
	}
	public function deleteUser($Id) {
		echo $Id;
		$int = ( int ) $Id;
		echo $int;
		$sth = $this->db->prepare ( "DELETE FROM `user_table` WHERE ID='$int'" );
		$sth->execute ();
	}
	public function viewAllUser() {
		$sth = $this->db->prepare ( "SELECT * FROM `user_table`" );
		$sth->execute ();
		$data = $sth->fetchAll ();
		return $data;
	}
	public function viewAllServiceMen() {
		$sth = $this->db->prepare ( "SELECT * FROM `service_men`" );
		$sth->execute ();
		$data = $sth->fetchAll ();
		return $data;
	}
	public function viewAllUnit() {
		$sth = $this->db->prepare ( "SELECT * FROM `unit`" );
		$sth->execute ();
		$data = $sth->fetchAll ();
		return $data;
	}
	public function checkUserId($data) {
		$sth = $this->db->prepare ( "SELECT `ID` FROM `user_table` where
				USER_ID=:userId" );
		$sth->execute ( array (
				':userId' => $data 
		) );
		$count = $sth->rowCount ();
		return $count;
	}
	public function checkUserById($data) {
		$sth = $this->db->prepare ( "SELECT `ID` FROM `user_table` where
				ID=:Id" );
		$sth->execute ( array (
				':Id' => $data 
		) );
		$count = $sth->rowCount ();
		return $count;
	}
	public function updateUserDetails() {
		$userId = $_POST ["userId"];
		$role = $_POST ['role'];
		$firstName = $_POST ['firstName'];
		$lastName = $_POST ['lastName'];
		$emailId = $_POST ['emailId'];
		$mobile = $_POST ['mobile'];
		$gender = $_POST ['gender'];
		$dob = $_POST ['dob'];
		$password = $_POST ['password'];
		$address1 = $_POST ['address1'];
		$address2 = $_POST ['address2'];
		$city = $_POST ['city'];
		$state = $_POST ['state'];
		$postCode = $_POST ['postCode'];
		$country = $_POST ['country'];
		$sth = $this->db->prepare ( "UPDATE `user_table` SET `ROLE`='$role',`FIRST_NAME`='$firstName',`LAST_NAME`='$lastName',`EMAIL_ID`='$emailId',`MOBILE`='$mobile',`GENDER`='$gender',`DOB`='$dob',`PASSWORD`='$password',`ADDRESS1`='$address1',`ADDRESS2`='$address2',`CITY`='$city',`STATE`='$state',`POST_CODE`='$postCode',`COUNTRY`='$country' where USER_ID='$userId'" );
		$sth->execute ();
	}
	public function getAdminDetails($adminId) {
		$sth = $this->db->prepare ( "SELECT * FROM `admin_table` where
				ADMIN_ID=:Id" );
		$sth->execute ( array (
				':Id' => $adminId 
		) );
		$data = $sth->fetchAll ();
		return $data;
	}
	public function updatePassword($newPass, $adminId) {
		$sth = $this->db->prepare ( "UPDATE `admin_table` SET `PASSWORD`='$newPass' WHERE ADMIN_ID='$adminId'" );
		$sth->execute ();
	}
	public function getAdminFaqs() {
		$sth = $this->db->prepare ( "SELECT * FROM faq" );
		$sth->execute ();
		$data = $sth->fetchAll ();
		return $data;
	}
	public function addFaq() {
		$sth = $this->db->prepare ( "INSERT INTO `faq`(`QUESTION`, `ANSWER`)
				VALUES (:question,:answer)" );
		$sth->execute ( array (
				':question' => $_POST ['question'],
				':answer' => $_POST ['answer'] 
		) );
	}
	public function checkQuestion($data) {
		$sth = $this->db->prepare ( "SELECT `QUESTION` FROM `faq` where
				QUESTION=:question" );
		$sth->execute ( array (
				':question' => $data 
		) );
		$count = $sth->rowCount ();
		return $count;
	}
	public function checkFaqById($data) {
		$sth = $this->db->prepare ( "SELECT `ID` FROM `faq` where
				ID=:id" );
		$sth->execute ( array (
				':id' => $data 
		) );
		$count = $sth->rowCount ();
		return $count;
	}
	public function getFaqById($Id) {
		$sth = $this->db->prepare ( "SELECT * FROM `faq` where
				ID=:Id" );
		$sth->execute ( array (
				':Id' => $Id 
		) );
		$data = $sth->fetchAll ();
		return $data;
	}
	public function updateFaqDetails() {
		$id = $_POST ['id'];
		$question = $_POST ["question"];
		$answer = $_POST ['answer'];
		
		$sth = $this->db->prepare ( "UPDATE `faq` SET `QUESTION`='$question',`ANSWER`='$answer' where ID='$id'" );
		$sth->execute ();
	}
	public function deleteFaq($Id) {
		echo $Id;
		$int = ( int ) $Id;
		echo $int;
		$sth = $this->db->prepare ( "DELETE FROM `faq` WHERE ID='$int'" );
		$sth->execute ();
	}
	
	
	public function createServiceMen($imagePath) {
		$sth = $this->db->prepare ( "INSERT INTO `service_men`(`NAME`, `ROLE`, `EMAIL_ID`, `MOBILE`, `GENDER`, `IMAGE_PATH`, `ADDRESS1`, `ADDRESS2`, `CITY`, `STATE`, `PINCODE`)
				VALUES (:name,:role,:emailId,:mobile,:gender,:imagePath,:addresss1,:address2,:city,:state,:pincode)" );
		$sth->execute ( array (
				':name' => $_POST ["name"],
				':role' => $_POST ['role'],
				':emailId' => $_POST ['emailId'],
				':mobile' => $_POST ['mobile'],
				':gender' => $_POST ['gender'],
				':imagePath' => $imagePath,
				':addresss1' => $_POST ['address1'],
				':address2' => $_POST ['address2'],
				':city' => $_POST ['city'],
				':state' => $_POST ['state'],
				':pincode' => $_POST ['postCode']
		) );
	}
	
	public function createUnit() {
		$sth = $this->db->prepare ( "INSERT INTO `unit`(`SERVICE_NAME`, `UNIT_TYPE`, `UNIT_DATA`, `PARTICULAR`, `PRICE`)
				VALUES (:serviceName,:unitType,:unitData,:particular,:price)" );
		$sth->execute ( array (
				':serviceName' => $_POST ["serviceName"],
				':unitType' => $_POST ['unitType'],
				':unitData' => $_POST ['unitData'],
				':particular' => $_POST ['particular'],
				':price' => $_POST ['price'] 
		) );
	}
	public function createUser() {
		$name = $_POST ['firstName'].$_POST ['lastName'];
		$userId=$_POST ["userId"];
		$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
		$pass = array (); // remember to declare $pass as an array
		$alphaLength = strlen ( $alphabet ) - 1; // put the length -1 in cache
		for($i = 0; $i < 8; $i ++) {
			$n = rand ( 0, $alphaLength );
			$pass [] = $alphabet [$n];
		}
		$password = implode ( $pass );
		$sth = $this->db->prepare ( "INSERT INTO `user_table`(`USER_ID`, `ROLE`, `FIRST_NAME`, `LAST_NAME`, `EMAIL_ID`, `MOBILE`, `GENDER`, `DOB`, `PASSWORD`, `ADDRESS1`, `ADDRESS2`, `CITY`, `STATE`, `POST_CODE`, `COUNTRY`)
				VALUES (:userId,:role,:firstName,:lastName,:emailId,:mobile,:gender,:dob,:password,:address1,:address2,:city,:state,:postCode,:country)" );
		$sth->execute ( array (
				':userId' => $_POST ["userId"],
				':role' => $_POST ['role'],
				':firstName' => $_POST ['firstName'],
				':lastName' => $_POST ['lastName'],
				':emailId' => $_POST ['emailId'],
				':mobile' => $_POST ['mobile'],
				':gender' => $_POST ['gender'],
				':dob' => $_POST ['dob'],
				':password' => $password,
				':address1' => $_POST ['address1'],
				':address2' => $_POST ['address2'],
				':city' => $_POST ['city'],
				':state' => $_POST ['state'],
				':postCode' => $_POST ['postCode'],
				':country' => $_POST ['country'] 
		) );
		
		$to = $_POST ['emailId'];
		$subject = "Registration Successful !";
		
		$message = "
		
<html>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' >
<title>Welcome to the Next Door</title>
<style type='text/css'>
html { -webkit-text-size-adjust:none; -ms-text-size-adjust: none;}
@media only screen and (max-device-width: 680px), only screen and (max-width: 680px) { 
	*[class='table_width_100'] {
		width: 96% !important;
	}
	*[class='border-right_mob'] {
		border-right: 1px solid #dddddd;
	}
	*[class='mob_100'] {
		width: 100% !important;
	}
	*[class='mob_center'] {
		text-align: center !important;
	}
	*[class='mob_center_bl'] {
		float: none !important;
		display: block !important;
		margin: 0px auto;
	}	
	.iage_footer a {
		text-decoration: none;
		color: #929ca8;
	}
	img.mob_display_none {
		width: 0px !important;
		height: 0px !important;
		display: none !important;
	}
	img.mob_width_50 {
		width: 40% !important;
		height: auto !important;
	}
}
.table_width_100 {
	width: 680px;
}
</style>
</head>

<body style='padding: 0px; margin: 0px;'>
<div id='mailsub' class='notification' align='center'>

<table width='100%' border='0' cellspacing='0' cellpadding='0' style='min-width: 320px;'><tr><td align='center' bgcolor='#eff3f8'>


<!--[if gte mso 10]>
<table width='680' border='0' cellspacing='0' cellpadding='0'>
<tr><td>
<![endif]-->

<table border='0' cellspacing='0' cellpadding='0' class='table_width_100' width='100%' style='max-width: 680px; min-width: 300px;'>
	<!--header -->
	<tr><td align='center' bgcolor='#eff3f8'>
		<!-- padding --><div style='height: 20px; line-height: 20px; font-size: 10px;'>&nbsp;</div>
		
		<!-- padding --><div style='height: 30px; line-height: 30px; font-size: 10px;'>&nbsp;</div>
	</td></tr>
	<!--header END-->

	<!--content 1 -->
	<tr><td align='center' bgcolor='#ffffff'>
		<table width='90%' border='0' cellspacing='0' cellpadding='0'>
			<tr><td align='center'>
				<!-- padding --><div style='height: 100px; line-height: 100px; font-size: 10px;'>&nbsp;</div>
				<div style='line-height: 44px;'>
					<font face='Arial, Helvetica, sans-serif' size='5' color='#57697e' style='font-size: 34px;'>
					<span style='font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #57697e;'>
						Welcome to the Next Door
					</span></font>
				</div>
				<!-- padding --><div style='height: 30px; line-height: 30px; font-size: 10px;'>&nbsp;</div>
			</td></tr>
			<tr><td align='center'>
				<div style='line-height: 30px;'>
					<font face='Arial, Helvetica, sans-serif' size='5' color='#4db3a4' style='font-size: 17px;'>
					<span style='font-family: Arial, Helvetica, sans-serif; font-size: 17px; color: #4db3a4;'>
						Hi <span style='color: orange;'>".$name.",</span> your registration is completed!
					</span></font>
				</div>
				<!-- padding --><div style='height: 35px; line-height: 35px; font-size: 10px;'>&nbsp;</div>
			</td></tr>
			<tr><td align='center'>
						<table width='80%' align='center' border='0' cellspacing='0' cellpadding='0'>
							<tr><td align='center'>
								<div style='line-height: 10px;'>
									<font face='Arial, Helvetica, sans-serif' size='4' color='#57697e' style='font-size: 16px;'>
									<span style='font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;'>
										Please use the below mentation credentials for Login to Next Door.
									</span></font>
								</div>
							</td></tr>
						</table>
				<!-- padding --><div style='height: 45px; line-height: 45px; font-size: 10px;'>&nbsp;</div>
			</td></tr>
			<tr>
			<td>
			<table style='margin-left: 63px;margin-bottom: 10px;'>
							<tr>
							<td >User Id:&nbsp; </td>
							<td align='center' style='color: brown;'>".$userId."</td>
							</tr>
							<tr>
							<td>Password: &nbsp;</td>
							<td align='center' style='color: brown;'>".$password."</td>
							</tr>
			</table>
			<br>
			</td>
			</tr>
		
			<!-- padding --><div style='height: 30px; line-height: 30px; font-size: 10px;'>&nbsp;</div>
			<tr><td align='center'>
						<table width='80%' align='center' border='0' cellspacing='0' cellpadding='0'>
							<tr><td align='center'>
								<div style='line-height: 24px;'>
									<font face='Arial, Helvetica, sans-serif' size='4' color='#57697e' style='font-size: 16px;'>
									<span style='font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;'>
										Lorem ipsum dolor sit amet consectetuer adipiscing elit, sed diam nonumy nibh elit esmod tincidunt ut laoreet dolore magna aliquam volutpat wisi enim ad minim veniam quis dolore.
									</span></font>
								</div>
							</td></tr>
						</table>
				<!-- padding --><div style='height: 45px; line-height: 45px; font-size: 10px;'>&nbsp;</div>
			</td></tr>
			<tr><td align='center'>
				<div style='line-height:24px;'>
					<a href='".URL."/index/index' target='_blank' style='color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 13px;'>
						<font face='Arial, Helvetica, sans-seri; font-size: 13px;' size='6' color='green'>
						Login To Next Door</font></a>
				</div>
				<!-- padding --><div style='height: 100px; line-height: 100px; font-size: 10px;'>&nbsp;</div>
			</td></tr>
		</table>		
	</td></tr>
	<!--content 1 END-->

	<!--links -->
	<tr><td align='center' bgcolor='#f9fafc'>
		<table width='100%' border='0' cellspacing='0' cellpadding='0'>
			<tr><td align='center'>
				<!-- padding --><div style='height: 30px; line-height: 30px; font-size: 10px;'>&nbsp;</div>
        <table width='80%' align='center' cellpadding='0' cellspacing='0'>
          <tr>
            <td align='center' valign='middle' style='font-size: 12px; line-height:22px;'>
            	<font face='Tahoma, Arial, Helvetica, sans-serif' size='2' color='#282f37' style='font-size: 12px;'>
								<span style='font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #5b9bd1;'>
		              <a href='#' target='_blank' style='color: #5b9bd1; text-decoration: none;'>GENERAL QUESTIONS</a>
		              &nbsp;&nbsp;&nbsp;&nbsp;</a>&nbsp;&nbsp;&nbsp;&nbsp;
		              <a href='#' target='_blank' style='color: #5b9bd1; text-decoration: none;'>TERMS &amp; CONDITIONS</a>
		              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		              <a href='#' target='_blank' style='color: #5b9bd1; text-decoration: none;'>UNSUBSCRIBE EMAIL</a>
              </span></font>
            </td>
          </tr>                                        
        </table>
			</td></tr>
			<tr><td><!-- padding --><div style='height: 30px; line-height: 30px; font-size: 10px;'>&nbsp;</div></td></tr>
		</table>		
	</td></tr>
	<!--links END-->

	<!--footer -->
	<tr><td class='iage_footer' align='center' bgcolor='#eff3f8'>
		<!-- padding --><div style='height: 40px; line-height: 40px; font-size: 10px;'>&nbsp;</div>	
		
		<table width='100%' border='0' cellspacing='0' cellpadding='0'>
			<tr><td align='center'>
				<font face='Arial, Helvetica, sans-serif' size='3' color='#96a5b5' style='font-size: 13px;'>
				<span style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #96a5b5;'>
					2016 &copy; Next Door. ALL Rights Reserved.
				</span></font>				
			</td></tr>			
		</table>
		
		<!-- padding --><div style='height: 50px; line-height: 50px; font-size: 10px;'>&nbsp;</div>	
	</td></tr>
	<!--footer END-->
</table>
<!--[if gte mso 10]>
</td></tr>
</table>
<![endif]-->
 
</td></tr>
</table>
			
</div> 
</body>
</html>
		
";
		
		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		
		// More headers
		$headers .= 'From: Next Door <info@nextdoorservice.in>' . "\r\n";
		
		mail ( $to, $subject, $message, $headers );
	}
}