<?php
class Index_model extends Model {
	function __construct() {
		parent::__construct();
		
	}
	
	
	public function updateContactUs() {
	
		 $sth=$this->db->prepare("INSERT INTO `contact_us`(`NAME`, `EMAIL_ID`, `MOBILE`, `SUBJECT`, `MESSAGE`)
				VALUES (:name,:emailId,:mobile,:subject,:message)");
		$sth->execute(array(
				':name'=>$_POST['name'],
				':emailId'=>$_POST['email'],
				':mobile'=>$_POST['mobile'],
				':subject'=>$_POST['subject'],
				':message'=>$_POST['message']
		)); 
	}
	
	
	
	public function plumber($service) {
		$sth = $this->db->prepare ( "SELECT * FROM `unit` WHERE
				SERVICE_NAME='$service'" );
		$sth->execute ();
		$data = $sth->fetchAll ();
		return $data;
	}
	public function getUnit($service,$unitType) {
		$sth = $this->db->prepare ( "SELECT DISTINCT `UNIT_DATA` FROM `unit` WHERE SERVICE_NAME='$service' AND UNIT_TYPE='$unitType'");
		$sth->execute ();
		$data = $sth->fetchAll ();
		return $data;
	}
	
	
	public function getFaqs() {
		$sth=$this->db->prepare("SELECT ID, QUESTION, ANSWER FROM faq");
		$sth->execute();
		$data=$sth->fetchAll();
		return $data;
	}
	
	public function checkUserByIdorEmail($id) {
		$sth = $this->db->prepare ( "SELECT COUNT(USER_ID), USER_ID, EMAIL_ID, PASSWORD FROM `user_table` WHERE 
				USER_ID=:Id OR EMAIL_ID=:email" );
		$sth->execute ( array (
				':Id' => $id,
				':email' => $id
		) );
		$data = $sth->fetchAll ();
		return $data;
	}
	public function viewAllServiceMen() {
		$sth = $this->db->prepare ( "SELECT * FROM `service_men`" );
		$sth->execute ();
		$data = $sth->fetchAll ();
		return $data;
	}
	
	public function bookNow() {
		$emailId=$_POST['email'];
		$name=$_POST['name'];
		$prefertime=$_POST['preferTime'];
		$six_digit_random_number = mt_rand(100000, 999999);
		$sth=$this->db->prepare("INSERT INTO `book_now`(BOOKING_NO, `NAME`, `CITY_NAME`, `SERVICE_NAME`, `MOBILE`, `EMAIL_ID`, `ADDRESS`, `PREFER_TIME`)
				VALUES (:bookNo,:name,:city,:service,:mobile,:email,:address,:prefertime)");
		$sth->execute(array(
				':bookNo'=>$six_digit_random_number,
				':name'=>$_POST['name'],
				':city'=>$_POST['city'],
				':service'=>$_POST['service'],
				':mobile'=>$_POST['mobile'],
				':email'=>$_POST['email'],
				':address'=>$_POST['address'],
				':prefertime'=>$_POST['preferTime']
		));
		
		$to = $emailId;
		$subject = "Booking Conformation !";
		
		$message = "
		
		

<html>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' >
<title>Welcome to the Next Door</title>
<style type='text/css'>
html { -webkit-text-size-adjust:none; -ms-text-size-adjust: none;}
@media only screen and (max-device-width: 680px), only screen and (max-width: 680px) { 
	*[class='table_width_100'] {
		width: 96% !important;
	}
	*[class='border-right_mob'] {
		border-right: 1px solid #dddddd;
	}
	*[class='mob_100'] {
		width: 100% !important;
	}
	*[class='mob_center'] {
		text-align: center !important;
	}
	*[class='mob_center_bl'] {
		float: none !important;
		display: block !important;
		margin: 0px auto;
	}	
	.iage_footer a {
		text-decoration: none;
		color: #929ca8;
	}
	img.mob_display_none {
		width: 0px !important;
		height: 0px !important;
		display: none !important;
	}
	img.mob_width_50 {
		width: 40% !important;
		height: auto !important;
	}
}
.table_width_100 {
	width: 680px;
}
</style>
</head>

<body style='padding: 0px; margin: 0px;'>
<div id='mailsub' class='notification' align='center'>

<table width='100%' border='0' cellspacing='0' cellpadding='0' style='min-width: 320px;'><tr><td align='center' bgcolor='#eff3f8'>


<!--[if gte mso 10]>
<table width='680' border='0' cellspacing='0' cellpadding='0'>
<tr><td>
<![endif]-->

<table border='0' cellspacing='0' cellpadding='0' class='table_width_100' width='100%' style='max-width: 680px; min-width: 300px;'>
	<!--header -->
	<tr><td align='center' bgcolor='#eff3f8'>
		<!-- padding --><div style='height: 20px; line-height: 20px; font-size: 10px;'>&nbsp;</div>
		
		<!-- padding --><div style='height: 30px; line-height: 30px; font-size: 10px;'>&nbsp;</div>
	</td></tr>
	<!--header END-->

	<!--content 1 -->
	<tr><td align='center' bgcolor='#ffffff'>
		<table width='90%' border='0' cellspacing='0' cellpadding='0'>
			<tr><td align='center'>
				<!-- padding --><div style='height: 100px; line-height: 100px; font-size: 10px;'>&nbsp;</div>
				<div style='line-height: 44px;'>
					<font face='Arial, Helvetica, sans-serif' size='5' color='#57697e' style='font-size: 34px;'>
					<span style='font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #57697e;'>
						Welcome to the Next Door
					</span></font>
				</div>
				<!-- padding --><div style='height: 30px; line-height: 30px; font-size: 10px;'>&nbsp;</div>
			</td></tr>
			<tr><td align='center'>
				<div style='line-height: 30px;'>
					<font face='Arial, Helvetica, sans-serif' size='5' color='#4db3a4' style='font-size: 17px;'>
					<span style='font-family: Arial, Helvetica, sans-serif; font-size: 17px; color: #4db3a4;'>
						Hi <span style='color: orange;'>".$name.",</span> Your booking request has been conformed !
					</span></font>
				</div>
				<!-- padding --><div style='height: 35px; line-height: 35px; font-size: 10px;'>&nbsp;</div>
			</td></tr>
			<tr><td align='center'>
						<table width='80%' align='center' border='0' cellspacing='0' cellpadding='0'>
							<tr><td align='center'>
								<div style='line-height: 10px;'>
									<font face='Arial, Helvetica, sans-serif' size='4' color='#57697e' style='font-size: 16px;'>
									<span style='font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;'>
										We will be in contact with you if we required further information.
									</span></font>
								</div>
							</td></tr>
						</table>
				<!-- padding --><div style='height: 45px; line-height: 45px; font-size: 10px;'>&nbsp;</div>
			</td></tr>
			<tr>
			<td>
			<table style='margin-left: 104px;margin-bottom: 10px;'>
							<tr>
							<td >Booking No:&nbsp; </td>
							<td align='center' style='color: brown;'>".$six_digit_random_number."</td>
							</tr>
							<tr>
							<td >Booking Time:&nbsp; </td>
							<td align='center' style='color: brown;'>".$prefertime."</td>
							</tr>
							
			</table>
			<br>
			</td>
			</tr>
		
			<!-- padding --><div style='height: 30px; line-height: 30px; font-size: 10px;'>&nbsp;</div>
			<tr><td align='center'>
						<table width='80%' align='center' border='0' cellspacing='0' cellpadding='0'>
							<tr><td align='center'>
								<div style='line-height: 24px;'>
									<font face='Arial, Helvetica, sans-serif' size='4' color='#57697e' style='font-size: 16px;'>
									<span style='font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;'>
										Lorem ipsum dolor sit amet consectetuer adipiscing elit, sed diam nonumy nibh elit esmod tincidunt ut laoreet dolore magna aliquam volutpat wisi enim ad minim veniam quis dolore.
									</span></font>
								</div>
							</td></tr>
						</table>
				<!-- padding --><div style='height: 45px; line-height: 45px; font-size: 10px;'>&nbsp;</div>
			</td></tr>
			<tr><td align='center'>
				
				<!-- padding --><div style='height: 100px; line-height: 100px; font-size: 10px;'>&nbsp;</div>
			</td></tr>
		</table>		
	</td></tr>
	<!--content 1 END-->

	<!--links -->
	<tr><td align='center' bgcolor='#f9fafc'>
		<table width='100%' border='0' cellspacing='0' cellpadding='0'>
			<tr><td align='center'>
				<!-- padding --><div style='height: 30px; line-height: 30px; font-size: 10px;'>&nbsp;</div>
        <table width='80%' align='center' cellpadding='0' cellspacing='0'>
          <tr>
            <td align='center' valign='middle' style='font-size: 12px; line-height:22px;'>
            	<font face='Tahoma, Arial, Helvetica, sans-serif' size='2' color='#282f37' style='font-size: 12px;'>
								<span style='font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #5b9bd1;'>
		              <a href='#' target='_blank' style='color: #5b9bd1; text-decoration: none;'>GENERAL QUESTIONS</a>
		              &nbsp;&nbsp;&nbsp;&nbsp;</a>&nbsp;&nbsp;&nbsp;&nbsp;
		              <a href='#' target='_blank' style='color: #5b9bd1; text-decoration: none;'>TERMS &amp; CONDITIONS</a>
		              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		              <a href='#' target='_blank' style='color: #5b9bd1; text-decoration: none;'>UNSUBSCRIBE EMAIL</a>
              </span></font>
            </td>
          </tr>                                        
        </table>
			</td></tr>
			<tr><td><!-- padding --><div style='height: 30px; line-height: 30px; font-size: 10px;'>&nbsp;</div></td></tr>
		</table>		
	</td></tr>
	<!--links END-->

	<!--footer -->
	<tr><td class='iage_footer' align='center' bgcolor='#eff3f8'>
		<!-- padding --><div style='height: 40px; line-height: 40px; font-size: 10px;'>&nbsp;</div>	
		
		<table width='100%' border='0' cellspacing='0' cellpadding='0'>
			<tr><td align='center'>
				<font face='Arial, Helvetica, sans-serif' size='3' color='#96a5b5' style='font-size: 13px;'>
				<span style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #96a5b5;'>
					2016 &copy; Next Door. ALL Rights Reserved.
				</span></font>				
			</td></tr>			
		</table>
		
		<!-- padding --><div style='height: 50px; line-height: 50px; font-size: 10px;'>&nbsp;</div>	
	</td></tr>
	<!--footer END-->
</table>
<!--[if gte mso 10]>
</td></tr>
</table>
<![endif]-->
 
</td></tr>
</table>
			
</div> 
</body>
</html>
				
		
		
";
		
		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		
		// More headers
		$headers .= 'From: Next Door <info@nextdoorservice.in>' . "\r\n";
		
		mail($to,$subject,$message,$headers);
	}	
	
	public function forgotPassword($emailId,$userId) {
		$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
		$pass = array(); //remember to declare $pass as an array
		$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
		for ($i = 0; $i < 8; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
		$password=implode($pass);
		$sth = $this->db->prepare ( "UPDATE `user_table` SET `PASSWORD`='$password',`USER_PASSWORD_STATUS`='false' WHERE EMAIL_ID='$emailId'" );
		$sth->execute ();
		
		$to = $emailId;
		$subject = "Reset Password !";
		
		$message = "
		

<html>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' >
<title>Welcome to the Next Door</title>
<style type='text/css'>
html { -webkit-text-size-adjust:none; -ms-text-size-adjust: none;}
@media only screen and (max-device-width: 680px), only screen and (max-width: 680px) { 
	*[class='table_width_100'] {
		width: 96% !important;
	}
	*[class='border-right_mob'] {
		border-right: 1px solid #dddddd;
	}
	*[class='mob_100'] {
		width: 100% !important;
	}
	*[class='mob_center'] {
		text-align: center !important;
	}
	*[class='mob_center_bl'] {
		float: none !important;
		display: block !important;
		margin: 0px auto;
	}	
	.iage_footer a {
		text-decoration: none;
		color: #929ca8;
	}
	img.mob_display_none {
		width: 0px !important;
		height: 0px !important;
		display: none !important;
	}
	img.mob_width_50 {
		width: 40% !important;
		height: auto !important;
	}
}
.table_width_100 {
	width: 680px;
}
</style>
</head>

<body style='padding: 0px; margin: 0px;'>
<div id='mailsub' class='notification' align='center'>

<table width='100%' border='0' cellspacing='0' cellpadding='0' style='min-width: 320px;'><tr><td align='center' bgcolor='#eff3f8'>


<!--[if gte mso 10]>
<table width='680' border='0' cellspacing='0' cellpadding='0'>
<tr><td>
<![endif]-->

<table border='0' cellspacing='0' cellpadding='0' class='table_width_100' width='100%' style='max-width: 680px; min-width: 300px;'>
	<!--header -->
	<tr><td align='center' bgcolor='#eff3f8'>
		<!-- padding --><div style='height: 20px; line-height: 20px; font-size: 10px;'>&nbsp;</div>
		
		<!-- padding --><div style='height: 30px; line-height: 30px; font-size: 10px;'>&nbsp;</div>
	</td></tr>
	<!--header END-->

	<!--content 1 -->
	<tr><td align='center' bgcolor='#ffffff'>
		<table width='90%' border='0' cellspacing='0' cellpadding='0'>
			<tr><td align='center'>
				<!-- padding --><div style='height: 100px; line-height: 100px; font-size: 10px;'>&nbsp;</div>
				<div style='line-height: 44px;'>
					<font face='Arial, Helvetica, sans-serif' size='5' color='#57697e' style='font-size: 34px;'>
					<span style='font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #57697e;'>
						Welcome to the Next Door
					</span></font>
				</div>
				<!-- padding --><div style='height: 30px; line-height: 30px; font-size: 10px;'>&nbsp;</div>
			</td></tr>
			<tr><td align='center'>
				<div style='line-height: 30px;'>
					<font face='Arial, Helvetica, sans-serif' size='5' color='#4db3a4' style='font-size: 17px;'>
					<span style='font-family: Arial, Helvetica, sans-serif; font-size: 17px; color: #4db3a4;'>
						Hi <span style='color: orange;'>".$userId.",</span> you have successfully reset password !
					</span></font>
				</div>
				<!-- padding --><div style='height: 35px; line-height: 35px; font-size: 10px;'>&nbsp;</div>
			</td></tr>
			<tr><td align='center'>
						<table width='80%' align='center' border='0' cellspacing='0' cellpadding='0'>
							<tr><td align='center'>
								<div style='line-height: 10px;'>
									<font face='Arial, Helvetica, sans-serif' size='4' color='#57697e' style='font-size: 16px;'>
									<span style='font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;'>
										Please use the below mentation credentials for Login to Next Door.
									</span></font>
								</div>
							</td></tr>
						</table>
				<!-- padding --><div style='height: 45px; line-height: 45px; font-size: 10px;'>&nbsp;</div>
			</td></tr>
			<tr>
			<td>
			<table style='margin-left: 63px;margin-bottom: 10px;'>
							<tr>
							<td >User Id:&nbsp; </td>
							<td align='center' style='color: brown;'>".$userId."</td>
							</tr>
							<tr>		
							<td>Email Id: &nbsp;</td>
							<td align='center' style='color: brown;'>".$emailId."</td>
							</tr>
							<tr>
							<td>Password: &nbsp;</td>
							<td align='center' style='color: brown;'>".$password."</td>
							</tr>
			</table>
			<br>
			</td>
			</tr>
		
			<!-- padding --><div style='height: 30px; line-height: 30px; font-size: 10px;'>&nbsp;</div>
			<tr><td align='center'>
						<table width='80%' align='center' border='0' cellspacing='0' cellpadding='0'>
							<tr><td align='center'>
								<div style='line-height: 24px;'>
									<font face='Arial, Helvetica, sans-serif' size='4' color='#57697e' style='font-size: 16px;'>
									<span style='font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;'>
										Lorem ipsum dolor sit amet consectetuer adipiscing elit, sed diam nonumy nibh elit esmod tincidunt ut laoreet dolore magna aliquam volutpat wisi enim ad minim veniam quis dolore.
									</span></font>
								</div>
							</td></tr>
						</table>
				<!-- padding --><div style='height: 45px; line-height: 45px; font-size: 10px;'>&nbsp;</div>
			</td></tr>
			<tr><td align='center'>
				<div style='line-height:24px;'>
					<a href='".URL."/index/index' target='_blank' style='color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 13px;'>
						<font face='Arial, Helvetica, sans-seri; font-size: 13px;' size='6' color='green'>
						Login To Next Door</font></a>
				</div>
				<!-- padding --><div style='height: 100px; line-height: 100px; font-size: 10px;'>&nbsp;</div>
			</td></tr>
		</table>		
	</td></tr>
	<!--content 1 END-->

	<!--links -->
	<tr><td align='center' bgcolor='#f9fafc'>
		<table width='100%' border='0' cellspacing='0' cellpadding='0'>
			<tr><td align='center'>
				<!-- padding --><div style='height: 30px; line-height: 30px; font-size: 10px;'>&nbsp;</div>
        <table width='80%' align='center' cellpadding='0' cellspacing='0'>
          <tr>
            <td align='center' valign='middle' style='font-size: 12px; line-height:22px;'>
            	<font face='Tahoma, Arial, Helvetica, sans-serif' size='2' color='#282f37' style='font-size: 12px;'>
								<span style='font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #5b9bd1;'>
		              <a href='#' target='_blank' style='color: #5b9bd1; text-decoration: none;'>GENERAL QUESTIONS</a>
		              &nbsp;&nbsp;&nbsp;&nbsp;</a>&nbsp;&nbsp;&nbsp;&nbsp;
		              <a href='#' target='_blank' style='color: #5b9bd1; text-decoration: none;'>TERMS &amp; CONDITIONS</a>
		              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		              <a href='#' target='_blank' style='color: #5b9bd1; text-decoration: none;'>UNSUBSCRIBE EMAIL</a>
              </span></font>
            </td>
          </tr>                                        
        </table>
			</td></tr>
			<tr><td><!-- padding --><div style='height: 30px; line-height: 30px; font-size: 10px;'>&nbsp;</div></td></tr>
		</table>		
	</td></tr>
	<!--links END-->

	<!--footer -->
	<tr><td class='iage_footer' align='center' bgcolor='#eff3f8'>
		<!-- padding --><div style='height: 40px; line-height: 40px; font-size: 10px;'>&nbsp;</div>	
		
		<table width='100%' border='0' cellspacing='0' cellpadding='0'>
			<tr><td align='center'>
				<font face='Arial, Helvetica, sans-serif' size='3' color='#96a5b5' style='font-size: 13px;'>
				<span style='font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #96a5b5;'>
					2016 &copy; Next Door. ALL Rights Reserved.
				</span></font>				
			</td></tr>			
		</table>
		
		<!-- padding --><div style='height: 50px; line-height: 50px; font-size: 10px;'>&nbsp;</div>	
	</td></tr>
	<!--footer END-->
</table>
<!--[if gte mso 10]>
</td></tr>
</table>
<![endif]-->
 
</td></tr>
</table>
			
</div> 
</body>
</html>
				
		
";
		
		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		
		// More headers
		$headers .= 'From: <jitumeher01@gmail.com>' . "\r\n";
		
		mail($to,$subject,$message,$headers);
		
	}
	
	public function registerDo() {
		Session::init();
		$userId=$_SESSION["userId"];
		$name=$_SESSION['name'];
		$password=$_SESSION['password'];
		$mobile=$_SESSION['mobile'];
		$emailId=$_SESSION['emailId'];
		$address=$_SESSION['address'];
		$state=$_SESSION['state'];
		$gender=$_SESSION['gender'];
		$sponsorId=$_SESSION['sponsorId'];
		$sponsorName="KItu";
		$sponsorMobile="1234567890"; 
		
		
		 $sth=$this->db->prepare("INSERT INTO `user_table`(`user_id`, `name`, `password`, `mobile`, `email_id`, `bank_acc_no`, `bank_acc_name`, `bank_name`, `branch_name`, `ifsc_code`, `imps_no`, `imps_mob`, `address`, `state`, `gender`, `sponsor_id`, `sponsor_name`, `sponsor_mob`)
				VALUES (:userId,:name,:password,:mobile,:emailId,:bankAccNo,:bankAccName,:bankName,:branchName,:ifsc,:address,:impsNo,:impsMobile,:state,:gender,:sponsorId,:sponsorName,:sponsorMobile)");
		$sth->execute(array(
				':userId'=>$userId,
				':name'=>$name,
				':password'=>$password,
				':mobile'=>$mobile,
				':emailId'=>$emailId,
				':bankAccNo'=>$_POST['bankAccNo'],
				':bankAccName'=>$_POST['bankAccName'],
				':bankName'=>$_POST['bankName'],
				':branchName'=>$_POST['branchName'],
				':ifsc'=>$_POST['ifsc'],
				':impsNo'=>$_POST['impsNo'],
				':impsMobile'=>$_POST['impsMobile'],
				':address'=>$address,
				':state'=>$state,
				':gender'=>$gender,
				':sponsorId'=>$sponsorId,
				':sponsorName'=>$sponsorName,
				':sponsorMobile'=>$sponsorMobile
		)); 
	}
	
	
}