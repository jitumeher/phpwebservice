<?php
class User_model extends Model {
	function __construct() {
		parent::__construct();
		
	}
	
	public function userLogin($userId,$email,$password) {
		$sth = $this->db->prepare ( "SELECT COUNT(USER_ID), USER_ID, ROLE, NAME  FROM `user_table` WHERE
				PASSWORD=:password AND USER_ID=:id OR EMAIL_ID=:email" );
		$sth->execute ( array (
				':id' => $userId,
				':email' => $email,
				':password' => $password
		) );
		$data = $sth->fetchAll ();
		return $data;
	}
	
	public function userProfile($id) {
		
		$sth = $this->db->prepare ("SELECT * FROM `user_table` where
				USER_ID=:Id" );
		$sth->execute ( array (
				':Id' => $id
		) );
		$data = $sth->fetchAll ();
		return $data;
	}
	
	public function userBankDetail($id) {
	
		$sth = $this->db->prepare ("SELECT * FROM `user_table` where
				USER_ID=:Id" );
		$sth->execute ( array (
				':Id' => $id
		) );
		$data = $sth->fetchAll ();
		return $data;
	}
	
}