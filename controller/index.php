<?php
class Index extends Controller {
	function __construct() {
		parent::__construct();
	}
	
	private $mobiles = array(
			1 => 'Apple iPhone 6S',
			2 => 'Samsung Galaxy S6',
			3 => 'Apple iPhone 6S Plus',
			4 => 'LG G4',
			5 => 'Samsung Galaxy S6 edge',
			6 => 'OnePlus 2');
	
	public function encodeJson($responseData) {
		$jsonResponse = json_encode($responseData);
		return $jsonResponse;
	}
	
	public function index(){
		$statusCode=200;
		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$this ->setHttpHeaders($requestContentType, $statusCode);
		
		if(strpos($requestContentType,'application/json') !== false){
			$response = $this->encodeJson( $this->model->viewAllServiceMen ());
			echo $response;
		}
		
	}
	public function getIndex(){
		require 'model/index_model.php';
		$model=new Index_model();
		$this->view->title="Next Door";
		$this->view->viewAllServiceMen = $model->viewAllServiceMen ();
		$this->view->render('public/index');
	}
	public function about(){
		$this->view->title="About Us";
		$this->view->render('public/about');
	}
	public function contact(){
	
		if ($_SERVER['REQUEST_METHOD']=='POST'){
			$this->model->updateContactUs();
			$this->view->title="Contact Us";
			$this->view->success="Thank you for filling out your information!";
			$this->view->render('public/contact');
		}else {
			$this->view->title="Contact Us";
			$this->view->render('public/contact');
		}
	}
	public function service(){
		$this->view->title="Next Door Service";
		$this->view->render('public/service');
	}
	public function price(){
		$this->view->title="Next Door Price";
		$this->view->render('public/price');
	}
	public function bookNow(){
		$this->model->bookNow();
		$this->view->success="Booking successfully done !";
		$this->view->render('public/bookNow');
	}
	public function plumber(){
		$service="PLUMBER";
		$unityTypeI="INSTALLATION";
		$unityTypeR="REPAIR";
		$this->view->getIUnit=$this->model->getUnit($service,$unityTypeI);
		$this->view->getRUnit=$this->model->getUnit($service,$unityTypeR);
		$this->view->plumber=$this->model->plumber($service);
		
		$this->view->render('public/plumber');
	}
	public function electrician(){
		$service="ELECTRICIAN";
		$unityTypeI="INSTALLATION";
		$unityTypeR="REPAIR";
		$this->view->getIUnit=$this->model->getUnit($service,$unityTypeI);
		$this->view->getRUnit=$this->model->getUnit($service,$unityTypeR);
		$this->view->plumber=$this->model->plumber($service);
	
		$this->view->render('public/electrician');
	}
	public function carpenter(){
		$service="CARPENTER";
		$unityTypeI="INSTALLATION";
		$unityTypeR="REPAIR";
		$this->view->getIUnit=$this->model->getUnit($service,$unityTypeI);
		$this->view->getRUnit=$this->model->getUnit($service,$unityTypeR);
		$this->view->plumber=$this->model->plumber($service);
	
		$this->view->render('public/carpenter');
	}
	public function acRefrigerator(){
		$service="ACREFRIGERATOR";
		$unityTypeI="INSTALLATION";
		$unityTypeR="REPAIR";
		$this->view->getIUnit=$this->model->getUnit($service,$unityTypeI);
		$this->view->getRUnit=$this->model->getUnit($service,$unityTypeR);
		$this->view->plumber=$this->model->plumber($service);
	
		$this->view->render('public/acRefrigerator');
	}
	
	public function faq(){
		$this->view->faqList=$this->model->getFaqs();
		$this->view->render('public/faq');
	}
	
	public function forgotPassword(){
		$id=$_POST['userId'];
		$count=0;
		$emailId='';
		$userId='';
		$data=$this->model->checkUserByIdorEmail($id,$id);
		foreach ($data as $user){
			$count=$user['COUNT(USER_ID)'];
			$emailId=$user['EMAIL_ID'];
			$userId=$user['USER_ID'];
		}
		if($count>0){
			$this->model->forgotPassword($emailId,$userId);
			$this->view->success="Password reset successful ckeck your mail ".$emailId." !";
			$this->view->render('public/forgotPassword');
			
		}else{
			$this->view->fail="Invalid credentials ".$id." !";
			$this->view->render('public/forgotPassword');
		}
		
	}
	
	public function login(){
		$this->view->render('public/login');
	}
	
	
	
	
}
