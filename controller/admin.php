<?php
class Admin extends Controller {
	function __construct() {
		parent::__construct ();
	}
	public function viewAllUser() {
		$this->adminview->viewAllUser = $this->model->viewAllUser ();
		$this->adminview->render ( 'admin/viewAllUser' );
	}
	public function deleteUser() {
		$userId = $_GET ["userId"];
		$newUserId = base64_decode ( urldecode ( $userId ) );
		$this->model->deleteUser ( $newUserId );
		$this->adminview->viewAllUser = $this->model->viewAllUser ();
		$this->adminview->render ( 'admin/viewAllUser' );
	}
	
	public function viewAllUnit() {
		$this->adminview->viewAllUnit = $this->model->viewAllUnit ();
		$this->adminview->render ( 'admin/viewAllUnit' );
	}
	public function createUnit() {
		if ($_SERVER ['REQUEST_METHOD'] == 'POST') {
			$this->model->createUnit ();
			$this->adminview->success = "Unit and Price created successfully !";
			$this->adminview->render ( 'admin/createUnit' );
		} else {
			$this->adminview->render ( 'admin/createUnit' );
		}
	}
	public function viewAllServiceMen() {
		$this->adminview->viewAllServiceMen = $this->model->viewAllServiceMen ();
		$this->adminview->render ( 'admin/viewAllServiceMen' );
	}
	public function createServiceMen() {
		if ($_SERVER ['REQUEST_METHOD'] == 'POST') {
			
			if(isset($_FILES['image'])){
				$errors= array();
				$file_name = $_FILES['image']['name'];
				$file_size =$_FILES['image']['size'];
				$file_tmp =$_FILES['image']['tmp_name'];
				$file_type=$_FILES['image']['type'];
				$tmp = explode('.', $_FILES['image']['name']);
				$file_ext = strtolower(end($tmp));
				$six_digit_random_number = mt_rand(100000, 999999);
				$newfilename=$six_digit_random_number .".".$file_ext;
				
				$expensions= array("jpeg","jpg","png");
			
				if(in_array($file_ext,$expensions)=== false){
					$errors[]="extension not allowed, please choose a JPEG or PNG file.";
				}
			
				if($file_size > 2097152){
					$errors[]='File size must be excately 2 MB';
				}
				echo $file_name;
				if(empty($errors)==true){
					move_uploaded_file($file_tmp,"upload/".$newfilename);
					echo "Success";
				}else{
					print_r($errors);
				}
			}
			
			
			$this->model->createServiceMen ($newfilename);
			$this->adminview->success = "Unit and Price created successfully !";
			$this->adminview->render ( 'admin/adminCreateSrviceMen' );
		} else {
			$this->adminview->render ( 'admin/adminCreateSrviceMen' );
		}
	}
	public function editUser() {
		if ($_SERVER ['REQUEST_METHOD'] == 'GET') {
			$userId = $_GET ["userId"];
			$newUserId = base64_decode ( urldecode ( $userId ) );
			if ($this->model->checkUserById ( $newUserId ) > 0) {
				$this->adminview->editUser = $this->model->editUser ( $newUserId );
				$this->adminview->render ( 'admin/adminEditUser' );
			} else {
				$this->adminview->viewAllUser = $this->model->viewAllUser ();
				$this->adminview->render ( 'admin/viewAllUser' );
			}
		} else if ($_SERVER ['REQUEST_METHOD'] == 'POST') {
			$userId = $_POST ["id"];
			
			$this->adminview->editUser = $this->model->editUser ( $userId );
			$this->model->updateUserDetails ();
			$this->adminview->success = "User Profile updated successfully !";
			$this->adminview->render ( 'admin/adminEditUser' );
		} else {
			$this->adminview->render ( 'admin/adminCreateUser' );
		}
	}
	public function login() {
		if ($_SERVER ['REQUEST_METHOD'] == 'GET') {
			$this->adminview->render ( 'admin/adminLogin' );
		} else if ($_SERVER ['REQUEST_METHOD'] == 'POST') {
			if ($this->model->adminLogin () > 0) {
				Session::init ();
				Session::set ( 'adminLoggedIn', true );
				Session::set ( 'adminId', $_POST ['adminId'] );
				$this->adminview->render ( 'admin/adminDashBoard' );
			} else {
				$this->adminview->render ( 'admin/adminLogin' );
			}
		} else {
			$this->adminview->render ( 'admin/adminLogin' );
		}
	}
	public function changePassword() {
		if ($_SERVER ['REQUEST_METHOD'] == 'GET') {
			$this->adminview->render ( 'admin/adminChangePassword' );
		} else if ($_SERVER ['REQUEST_METHOD'] == 'POST') {
			$oldPass = $_POST ["oldPass"];
			$newPass = $_POST ["newPass"];
			Session::init ();
			$adminId = Session::get ( "adminId" );
			$this->adminview->adminDetails = $this->model->getAdminDetails ( $adminId );
			foreach ( $this->adminview->adminDetails as $admin ) {
				$dbPass = $admin ['PASSWORD'];
			}
			if ($oldPass == $dbPass) {
				$this->model->updatePassword ( $newPass, $adminId );
				$this->adminview->success = "Password updated successfully !";
				$this->adminview->render ( 'admin/adminChangePassword' );
			} else {
				$this->adminview->error = "Wrong Old Password !";
				$this->adminview->render ( 'admin/adminChangePassword' );
			}
		} else {
			$this->adminview->render ( 'admin/adminChangePassword' );
		}
	}
	public function createUser() {
		if ($_SERVER ['REQUEST_METHOD'] == 'GET') {
			$this->adminview->render ( 'admin/adminCreateUser' );
		} else if ($_SERVER ['REQUEST_METHOD'] == 'POST') {
			$userId = $_POST ["userId"];
			if ($this->model->checkUserId ( $userId ) == 0) {
				$this->model->createUser ();
				$this->adminview->success = "User Create successfully !";
				$this->adminview->render ( 'admin/adminCreateUser' );
			} else {
				$this->adminview->error = "User Id exists use different one !";
				$this->adminview->render ( 'admin/adminCreateUser' );
			}
		} else {
			$this->adminview->render ( 'admin/adminCreateUser' );
		}
	}
	public function adminDashboard() {
		$this->adminview->render ( 'admin/adminDashBoard' );
	}
	public function questionAnswer() {
		if ($_SERVER ['REQUEST_METHOD'] == 'GET') {
			$this->adminview->faqList = $this->model->getAdminFaqs ();
			$this->adminview->render ( 'admin/questionAnswer' );
		} else if ($_SERVER ['REQUEST_METHOD'] == 'POST') {
			$question = $_POST ["question"];
			if ($this->model->checkQuestion ( $question ) == 0) {
				$this->model->addFaq ();
				$this->adminview->success = "Question & Answer Create successfully !";
				$this->adminview->faqList = $this->model->getAdminFaqs ();
				$this->adminview->render ( 'admin/questionAnswer' );
			} else {
				$this->adminview->error = "Question exists create different one !";
				$this->adminview->faqList = $this->model->getAdminFaqs ();
				$this->adminview->render ( 'admin/questionAnswer' );
			}
		} else {
			$this->adminview->faqList = $this->model->getAdminFaqs ();
			$this->adminview->render ( 'admin/questionAnswer' );
		}
	}
	public function editFaq() {
		if ($_SERVER ['REQUEST_METHOD'] == 'GET') {
			$id = $_GET ["id"];
			$newId = base64_decode ( urldecode ( $id ) );
			if ($this->model->checkFaqById ( $newId ) > 0) {
				$this->adminview->faq = $this->model->getFaqById ( $newId );
				$this->adminview->render ( 'admin/editFaq' );
			} else {
				$this->adminview->faqList = $this->model->getAdminFaqs ();
				$this->adminview->render ( 'admin/questionAnswer' );
			}
		} else if ($_SERVER ['REQUEST_METHOD'] == 'POST') {
			$id = $_POST ["id"];
			$this->model->updateFaqDetails ();
			$this->adminview->updateSuccess = "Faq details updated successfully !";
			$this->adminview->faqList = $this->model->getAdminFaqs ();
			$this->adminview->render ( 'admin/questionAnswer' );
		} else {
			$this->adminview->faqList = $this->model->getAdminFaqs ();
			$this->adminview->render ( 'admin/questionAnswer' );
		}
	}
	public function deleteFaq() {
		$id = $_GET ["id"];
		$newId = base64_decode ( urldecode ( $id ) );
		$this->model->deleteFaq ( $newId );
		$this->adminview->updateSuccess = "Faq deleted successfully !";
		$this->adminview->faqList = $this->model->getAdminFaqs ();
		$this->adminview->render ( 'admin/questionAnswer' );
	}
	public function adminLogout() {
		Session::init ();
		Session::destroy ();
		$this->adminview->render ( 'admin/adminLogin' );
		exit ();
	}
}