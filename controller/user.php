<?php
class User extends Controller {
	function __construct() {
		parent::__construct ();
	}
	public function encodeJson($responseData) {
		$jsonResponse = json_encode ( $responseData );
		return $jsonResponse;
	}
	public function userLogin() {
		$post = file_get_contents ( 'php://input' );
		$post = json_decode ( $post, true );
		$statusCode = 200;
		$requestContentType = $_SERVER ['HTTP_ACCEPT'];
		$this->setHttpHeaders ( $requestContentType, $statusCode );
		
		if (strpos ( $requestContentType, 'application/json' ) !== false) {
			$response = $this->encodeJson ( $this->model->userLogin ( $post ['userId'], $post ['emailId'], $post ['password'] ) );
			echo $response;
		}
	}
	public function userProfile() {
		$statusCode = 200;
		$requestContentType = $_SERVER ['HTTP_ACCEPT'];
		$this->setHttpHeaders ( $requestContentType, $statusCode );
		
		if (strpos ( $requestContentType, 'application/json' ) !== false) {
			 $response = $this->encodeJson( $this->model->userProfile ($_GET['userId']));
			 echo $response;
			 
		}
	}
}